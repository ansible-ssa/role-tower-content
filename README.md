# Ansible Role: Tower Content

This role will configure an existing Ansible Tower Server. It will setup content which can be used as a foundation for Ansible Tower Demos or as a blueprint.

## Tower Content

### Inventories

The role will setup a GCP and AWS dynamic inventory.

### Machine Credentials

The role will add an "ansible" machine credential which will be used to log into managed nodes during Playbook execution.

### GitLab Projects

The role will setup a set of GitLab repositories which are used by the Job Templates.

### Job Templates

A few examples to setup and remove a Web Server. Also a set of Job Templates to deploy an Ansible Tower Server, install and configure it (in fact calling this role, which will allow easier development of future roles).

### License

Will install and enable a user provided Ansible Tower license key.

### Python virtual environment

Sets up an venv for Google Cloud, but is currently not used by any other Job.

## How to use

The role is optimized to be used from the [Playbook Tower](https://gitlab.com/redhat-cop/ansible-ssa/playbook-tower) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

  - name: Configure Tower
    include_role:
      name: tower_content

Add the role to your requirements.yml or import it manually first.

## Variables

This role is using a list of variables to configure the Tower Server. Check the [defaults](defaults/main.yml) and adjust them to your needs.
